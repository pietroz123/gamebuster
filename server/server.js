const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

require('dotenv').config();

const uri = process.env.MONGODB_URI;
const db = mongoose
    .connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
    });

const app = express();

// CORS
app.use(cors());

// Middlewares
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
    express.json({
        // We need the raw body to verify webhook signatures.
        // Let's compute it only when hitting the Stripe webhook endpoint.
        verify: function (req, res, buf) {
            if (req.originalUrl.startsWith("/api/stripe/webhook")) {
                req.rawBody = buf.toString();
            }
        },
    })
);
app.use(bodyParser.json());

// Routes
app.use('/api/users', require('./routes/api/users'));
app.use('/api/stripe', require('./routes/api/stripe'));
app.use('/api/alugueis', require('./routes/api/alugueis'));

// Start the server
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server up and running on port ${port}`));
