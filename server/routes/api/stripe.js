const router = require('express').Router();

const StripeController = require('../../controllers/stripe');

/**
 * @route   POST /api/stripe/create-payment-intent
 * @desc    Create a payment intent
 * @access  PUBLIC
 */
router.route('/create-payment-intent')
    .post(StripeController.createPaymentIntent);

/**
 * @route   GET /api/stripe/checkout-session
 * @desc    Get the checkout session information
 * @access  PUBLIC
 */
router.route('/checkout-session')
    .get(StripeController.getSession);

/**
 * @route   POST /api/stripe/create-checkout-session
 * @desc    Create a checkout session
 * @access  PUBLIC
 */
router.route('/create-checkout-session')
    .post(StripeController.createSession);

/**
 * @route   POST /api/stripe/webhook
 * @desc    Handle webhook calls
 * @access  PUBLIC
 */
router.route('/webhook')
    .post(StripeController.webhook);

module.exports = router;