const router = require('express').Router();

const AluguelController = require('../../controllers/alugueis');

/**
 * @route   GET /api/alugueis
 * @desc    Get the checkout session information
 * @access  PUBLIC
 */
router.route('/')
    .get(AluguelController.getAlugueis);

/**
 * @route   POST /api/alugueis/criar-alguel
 * @desc    Adiciona um aluguel ao usuário especificado
 * @access  PUBLIC
 */
router.route('/criar-aluguel')
    .post(AluguelController.criarAluguel);

module.exports = router;