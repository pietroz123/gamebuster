const db = require('../db');
const User = require('../models/User');

module.exports = {

    /**
     * Recupera os alugueis do usuário
     */
    getAlugueis: async (req, res, next) => {

        try {
            const results = await db.query('SELECT * FROM salesforce.GB_Aluguel__c');
            res.send(results);
        } catch (error) {
            res.status(400).json(error);
        }

    },

    /**
     * Cria um aluguel para o usuário especificado na propriedade "userId" no body
     */
    criarAluguel: async (req, res, next) => {

        const { items, userId } = req.body;

        const user = await User.findOne({ _id: userId });

        // TODO: lógica para verificar se o usuário atingiu o máximo de aluguéis baseado no seu plano

        try {
            const seen = new Set();
            const STATUS = 'Em trânsito';

            // Adiciona status
            items.map(item => {
                item.status = STATUS;
            });

            /**
             * Atribui os aluguéis ao usuário
             */
            user.alugueis = [...user.alugueis, ...items];

            // Remove itens duplicados
            user.alugueis = user.alugueis.filter(el => {
                const duplicate = seen.has(el.id);
                seen.add(el.id);
                return !duplicate;
            });

            const updatedUser = await user.save();

            /**
             * Cria os registros no Salesforce
             */
            const queryInsert = 'INSERT INTO salesforce.GB_Aluguel__c (Customer_Name__c, Game_Name__c, Status__c) VALUES ($1, $2, $3)';

            for (let i = 0; i < updatedUser.alugueis.length; i++) {
                let item = updatedUser.alugueis[i];
                await db.query(queryInsert, [updatedUser.auth0.name, item.name, STATUS]);
            }

            res.send(updatedUser);

        } catch (err) {
            console.error(err);
        }

    }

}