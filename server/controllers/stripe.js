const stripe = require('stripe')(process.env.STRIPE_TEST_SECRET_KEY);
const User = require('../models/User');

module.exports = {

    /**
     * Cria uma intenção de pagamento pelo usuário
     */
    createPaymentIntent: async (req, res, next) => {

        let { amount, currency, payment_method_types, receipt_email } = req.body;

        const paymentIntent = await stripe.paymentIntents.create({
            amount: amount,
            currency: currency,
            payment_method_types: payment_method_types,
            receipt_email: receipt_email,
        });

        if (paymentIntent) {
            res.json({ paymentIntent });
        }

    },

    /**
     * Stripe recomenda criar uma Session para cada tentativa de pagamento
     */
    createSession: async (req, res, next) => {

        const { priceId, stripeCustomerId } = req.body;

        // See https://stripe.com/docs/api/checkout/sessions/create
        // for additional parameters to pass.
        try {
            const session = await stripe.checkout.sessions.create({
                mode: "subscription",
                payment_method_types: ["card"],
                line_items: [
                    { price: priceId, quantity: 1, },
                ],
                customer: stripeCustomerId,

                // {CHECKOUT_SESSION_ID} is a string literal; do not change it!
                // the actual Session ID is returned in the query parameter when your customer
                // is redirected to the success page.
                success_url: `${process.env.APP_URL}/success/{CHECKOUT_SESSION_ID}`,
                cancel_url: `${process.env.APP_URL}/`,
            });

            res.send({
                sessionId: session.id,
            });

        } catch (e) {
            res.status(400);
            return res.send({
                error: {
                    message: e.message,
                }
            });
        }

    },

    /**
     * Recupera as informações da sessão de checkout a partir do seu ID
     */
    getSession: async (req, res, next) => {
        const { sessionId } = req.query;

        try {
            const session = await stripe.checkout.sessions.retrieve(sessionId);
            res.send(session);
        } catch (e) {
            res.status(400);
            return res.send({
                error: {
                    message: `Não foi possível recuperar a sessão de checkout : ${e.message}`,
                }
            });
        }

    },

    /**
     * Webhook para tratar os eventos gerados pelo Stripe
     */
    webhook: async (req, res, next) => {
        console.log('StripeController -> webhook()');

        let eventType;
        console.log("eventType\n", eventType);

        // Check if webhook signing is configured.
        const webhookSecret = process.env.STRIPE_WEBHOOK_SECRET;
        if (webhookSecret) {
            // Retrieve the event by verifying the signature using the raw body and secret.
            let event;
            let signature = req.headers["stripe-signature"];
            console.log("signature", signature);

            try {
                event = stripe.webhooks.constructEvent(
                    req.rawBody,
                    signature,
                    process.env.STRIPE_WEBHOOK_SECRET
                );
            } catch (err) {
                console.log(`⚠️  Webhook signature verification failed : ${err.message}`);
                return res.status(400).send({
                    message: `A verificação do webhook falhou. : ${err.message}`
                });
            }
            // Extract the object from the event.
            data = event.data;
            eventType = event.type;
        } else {
            // Webhook signing is recommended, but if the secret is not configured in `config.js`,
            // retrieve the event data directly from the request body.
            data = req.body.data;
            eventType = req.body.type;
        }


        /**
         * Trata cada tipo de evento
         */
        switch (eventType) {
            case 'checkout.session.completed':
                console.log(`🔔  Payment received!`);
                // Payment is successful and the subscription is created.
                // You should provision the subscription.

                let { subscription, customer } = data.object;
                const stripeSubscription = await stripe.subscriptions.retrieve(subscription);
                const stripeProduct = await stripe.products.retrieve(stripeSubscription.plan.product);

                // cria o plano
                let plan = {
                    id: stripeProduct.id,
                    name: stripeProduct.name,
                }

                // atribui o plano ao usuário
                await User.findOneAndUpdate({ stripeCustomerId: customer }, { plan });

                break;
            case 'invoice.paid':
                // Continue to provision the subscription as payments continue to be made.
                // Store the status in your database and check when a user accesses your service.
                // This approach helps you avoid hitting rate limits.
                break;
            case 'invoice.payment_failed':
                // The payment failed or the customer does not have a valid payment method.
                // The subscription becomes past_due. Notify your customer and send them to the
                // customer portal to update their payment information.
                break;
            default:
            // Unhandled event type
        }

        res.sendStatus(200);

    }

}