const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const aluguelSchema = new Schema({
    id: { type: Number },
    name: { type: String, },
    image: { type: String, },
    status: { type: String, }
});

// Create Model
const Aluguel = mongoose.model('aluguel', aluguelSchema);

// Export Model
module.exports = Aluguel;