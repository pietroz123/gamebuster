/**
 * Remove propriedades falsas (null, undefines, '', NaN...) de um objeto
 * @param {*} obj
 */
export function removeFalsyProps(obj) {
    for (var propName in obj) {
        if (!obj[propName]) {
        delete obj[propName];
        }
    }
    return obj;
}