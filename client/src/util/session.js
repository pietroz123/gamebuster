export default {
    setAuthentication(user) {
        window.localStorage.setItem('authUser', JSON.stringify(user));
    },
    getAuthUser() {
        const auth = JSON.parse(window.localStorage.getItem('authUser'));
        return auth || null;
    },
    clear() {
        window.localStorage.clear();
    }
}
