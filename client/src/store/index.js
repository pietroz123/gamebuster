import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from 'vuex-persistedstate';

// Importar módulos
import authModule from "./auth";
import stripeModule from "./stripe";
import gamesModule from "./games";
import cartModule from "./shopping-cart";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    plugins: [
        createPersistedState({
            paths: ['auth', 'cart'],
        }),
    ],
    mutations: {},
    actions: {},
    modules: {
        auth: authModule,
        stripe: stripeModule,
        games: gamesModule,
        cart: cartModule,
    },
});
