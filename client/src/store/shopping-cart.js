import axios from 'axios';

export default {
    namespaced: true,
    state: {
        cartItems: [],
        checkoutStatus: null
    },
    getters: {
        cartProducts: (state, getters, rootState) => {
            return state.cartItems.map(({ id, quantity, name, image }) => {
                // const game = rootState.products.all.find(game => game.id === id)
                return {
                    // title: game.title,
                    // price: game.price,
                    id,
                    name,
                    image,
                    quantity
                }
            })
        },

        // cartTotalPrice: (state, getters) => {
        //     return getters.cartProducts.reduce((total, game) => {
        //         return total + game.price * game.quantity
        //     }, 0)
        // }
    },
    mutations: {
        pushProductToCart(state, { id, name, image }) {
            state.cartItems.push({
                id,
                name,
                image,
                quantity: 1
            })
        },

        incrementItemQuantity(state, { id }) {
            const cartItem = state.cartItems.find(item => item.id === id)
            cartItem.quantity++
        },

        setCartItems(state, { cartItems }) {
            state.cartItems = cartItems
        },

        setCheckoutStatus(state, status) {
            state.checkoutStatus = status
        }
    },
    actions: {
        checkout({ commit, state, rootState }, products) {
            const savedCartItems = [...state.cartItems]
            commit('setCheckoutStatus', null)

            // empty cart
            commit('setCartItems', { cartItems: [] })

            // recupera o id do usuário no state do módulo de autenticação
            const userId = rootState.auth.authUser._id;

            return axios
                .post(`${process.env.VUE_APP_SERVER_URL}/api/alugueis/criar-aluguel`, {
                    items: savedCartItems,
                    userId
                })
                .then((res) => {
                    const updatedUser = res.data;

                    // atualiza a store com o usuário atualizado
                    commit('auth/setAuthentication', updatedUser, { root: true });
                })
                .catch((error) => {
                    console.log(error);
                });
        },

        addProductToCart({ state, commit }, game) {
            commit('setCheckoutStatus', null)

            const cartItem = state.cartItems.find(item => item.id === game.id)
            if (!cartItem) {
                commit('pushProductToCart', { id: game.id, name: game.name, image: game.background_image })
            }
            else {
                // mostrar alerta de que o jogo já foi adicionado
            }
            // else {
            //     commit('incrementItemQuantity', cartItem)
            // }
        }
    }
}