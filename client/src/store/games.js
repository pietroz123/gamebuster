import axios from 'axios';
import store from '.';
import { removeFalsyProps } from '../util/objectFilters';

export default {
    state: {
        games: [],
        currentParams: { key: '2a5059ab666f420082c4f8f31b076451' },
    },
    getters: {
        games: state => {
            return state.games;
        }
    },
    mutations: {
        setGames(state, games) {
            state.games = games;
        }
    },
    actions: {
        getGames(context, customParams) {
            let params = removeFalsyProps({ ...context.state.currentParams, ...customParams });
            context.state.currentParams = params;

            return axios
                .get('https://api.rawg.io/api/games', {
                    params: params,
                })
                .then((response) => {
                    store.commit('setGames', response.data);
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }
}