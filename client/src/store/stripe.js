import axios from 'axios';

export default {
	state: {
		session: null
	},
	getters: {
		checkoutSession: state => {
			return state.session;
		}
	},
	mutations: {
	},
	actions: {
		createCheckoutSession(context, data) {
			const { priceId, stripeCustomerId } = data;

			return axios
				.post(`${process.env.VUE_APP_SERVER_URL}/api/stripe/create-checkout-session`, {
					priceId: priceId,
					stripeCustomerId: stripeCustomerId
				})
				.then((response) => {
					context.state.session = response.data.sessionId;
				})
				.catch((error) => {
					console.log(error);
				});
		},
		getCheckoutSession(context, sessionId) {
			return axios
				.get(`${process.env.VUE_APP_SERVER_URL}/api/stripe/checkout-session`, {
					params: {
						sessionId: sessionId
					}
				})
				.then((response) => {
					return response;
				})
				.catch((error) => {
					console.log(error);
				});
		}
	}
}