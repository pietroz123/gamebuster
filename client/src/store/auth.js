import axios from 'axios';
import session from '../util/session'

export default {
    namespaced: true,
    state: {
        authUser: null, // ou session.getAuthUser(), sem vuex-persistedstate
    },
    getters: {
        authUser: state => {
            return state.authUser;
        },
    },
    mutations: {
        setAuthentication(state, user) {
            state.authUser = user;
            session.setAuthentication(user);
        },
    },
    actions: {
        handleAuth0Login(context, user) {
            if (user) {
                return axios
                    .post(`${process.env.VUE_APP_SERVER_URL}/api/users/auth0Hook`, {
                        user: user
                    })
                    .then((response) => {
                        let { data } = response;
                        user = {
                            ...user,
                            ...data, // adiciona as informações locais
                        };

                        context.commit('setAuthentication', user);
                    })
                    .catch((error) => {
                        console.error(error);
                    });
            }
        },
    }
}