import Vue from "vue";
import App from "./App.vue";
import router from './router'
import store from './store'

import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(fas, far, fab);
Vue.component('vue-fontawesome', FontAwesomeIcon);

import Buefy from 'buefy';
import 'buefy/dist/buefy.css'

// Import the plugin here
import { Auth0Plugin } from "./auth";

Vue.use(Buefy, { defaultIconComponent: 'vue-fontawesome', defaultIconPack: 'fas' });

// Install the authentication plugin here
Vue.use(Auth0Plugin, {
  onRedirectCallback: appState => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    );
  }
});


Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");