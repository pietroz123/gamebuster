import Vue from 'vue'
import VueRouter from 'vue-router'
import { authGuard } from "../auth/authGuard";

// imports
import Home from '../views/Home.vue'
import Auth0Callback from '../views/Auth0Callback.vue'
import Dashboard from '../views/Dashboard.vue'
import Catalogo from '../views/Catalogo.vue'
import Checkout from '../views/Checkout.vue'
import PurchasePlan from '../views/PurchasePlan.vue'
import SubscriptionSuccess from '../views/SubscriptionSuccess.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
		path: '/auth0callback',
		name: 'Auth0callback',
		component: Auth0Callback
	},
  {
    path: '/catalogo',
    name: 'Catalogo',
    component: Catalogo
  },
  {
    path: '/checkout',
    name: 'Checkout',
    component: Checkout,
    beforeEnter: authGuard
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: "/dashboard",
    name: "dashboard",
    component: Dashboard,
    beforeEnter: authGuard
  },
  {
    path: "/buy-plan/:priceId",
    name: "buy-plan",
    component: PurchasePlan,
    beforeEnter: authGuard
  },
  {
    path: "/success/:sessionId",
    name: "success",
    component: SubscriptionSuccess
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
